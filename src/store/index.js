import Vue from 'vue';
import Vuex from 'vuex';
import Store from '../model/store.class';

Vue.use(Vuex);
import APIService from './../model/API/apiController';
const Api = new APIService();

const store = new Vuex.Store({
    state: {
        almacen: new Store(1),
    },

    mutations: {
        insertarDatos(state, producto) {
            state.almacen.addProductWUnits(
                producto.id,
                producto.name,
                producto.price,
                producto.units,
            );
        },
        incrementOne(state, id) {
            state.almacen.changeProductUnits(id, +1);
        },
        decrementOne(state, id) {
            state.almacen.changeProductUnits(id, -1);
        },
        deleteProd(state, id) {
            state.almacen.delProduct(id);
        },
        newProd(state, producto) {
            state.almacen.addProduct(
                producto.data.id,
                producto.data.name,
                producto.data.price,
            );
        },
        editProd(state, producto) {
            state.almacen.replaceProduct(
                producto.data.id,
                producto.data.name,
                producto.data.price,
                producto.data.units,
            );
        },
    },
    getters: {
        calcularImporte: state => {
            return state.almacen.totalImport();
        },
    },
    actions: {
        getAll(context) {
            Api.getAll().then(productos => {
                productos.data.forEach(producto => {
                    context.commit('insertarDatos', producto);
                });
            }).catch(error => {
                alert(error);
            });
        },
        incrementOneUnit(context, id) {
            const producto = context.state.almacen.findProduct(id);
            context.commit('incrementOne', producto.id);

            Api.editProd(producto).catch(() => {
                context.commit('decrementOne', producto.id);
            });
        },
        decrementOneUnit(context, id) {
            const producto = context.state.almacen.findProduct(id);
            context.commit('decrementOne', producto.id);
            Api.editProd(producto).catch(() => {
                context.commit('incrementOne', producto.id);
            });
        },
        deleteProd(context, producto) {
            if (confirm(`¿Está seguro de que quiere eliminar el producto: ${producto.name}?`, )) {
                if (producto.units > 0) {
                    if (confirm(`Se eliminará el producto ${producto.name} el cual tiene ${producto.units} unidades.`)) {
                        Api.delProd(producto.id).then(() => {
                            context.commit('deleteProd', producto.id);
                        }).catch(error => {
                            alert(error);
                        });
                    }
                } else {
                    Api.delProd(producto.id).then(() => {
                        context.commit('deleteProd', producto.id);
                    }).catch(error => {
                        alert(error);
                    });
                }
            }
        },
        newProd(context, producto) {
            // añade un producto a la tabla.
            Api.addProd(producto).then(productReceived => {
                context.commit('newProd', productReceived);
            }).catch(error => {
                alert(error);
            });
        },
        editProd(context, prod) {
            Api.editProd(prod).then(productReceived => {
                context.commit('editProd', productReceived);
            }).catch(error => {
                alert(error);
            });
        },
    },
});

export default store;
