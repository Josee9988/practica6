import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import VeeValidate, {
  Validator
} from 'vee-validate'
import es from 'vee-validate/dist/locale/es'
import store from '@/store'


const Veeconfig = {
  locale: 'es_ES',
  events: 'blur',
  fieldsBagName: 'veeFields',
  inject: true,
};
Validator.localize({
  es_ES: es
});

Vue.use(VeeValidate, Veeconfig)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
