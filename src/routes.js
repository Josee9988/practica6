import Vue from 'vue'
import Router from 'vue-router'

import AppForm from './components/AppForm.vue';
import AppTable from './components/AppTable.vue';
import AppHome from './components/AppHome.vue';
import AppAbout from './components/AppAbout.vue';
import ShowProduct from './components/ShowProduct.vue';
import NotFound from './components/NotFound.vue';

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [{
            path: '/',
            name: 'home',
            component: AppHome
        }, {
            path: '/about',
            name: 'about',
            component: AppAbout
        }, {
            path: '/newProduct',
            component: AppForm
        }, {
            path: '/showProducts',
            component: AppTable
        }, {
            path: '/editProduct/:id',
            name: 'editProduct',
            component: AppForm,
            props: true
        },
        {
            path: '/showSingleProduct/:id',
            name: 'showSingleProduct',
            component: ShowProduct,
            props: true
        },
        {
            path: '/not-found',
            name: '404',
            component: NotFound,
        },
        {
            path: '*',
            redirect: {
                name: '404',
            },
        }
    ],
})
